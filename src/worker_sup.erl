-module(worker_sup).

-behaviour(supervisor).

-export([start_link/0, start_worker/0]).

-export([init/1]).

-define(CHILD_SPEC, #{id => worker, start => {worker, start_link, []}}).

start_worker() ->
  supervisor:start_child(self(), ?CHILD_SPEC).

start_link() ->
  supervisor:start_link(?MODULE, []).

init([]) ->
  SupFlags = #{strategy => simple_one_for_one, period => 5, intensity => 2},
  Children = [?CHILD_SPEC],
  {ok, {SupFlags, Children}}.
