-module(gen_example_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: #{id => Id, start => {M, F, A}}
%% Optional keys are restart, shutdown, type, modules.
%% Before OTP 18 tuples must be used to specify a child. e.g.
%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
  SupFlags = #{strategy => one_for_one, period => 5, intensity => 2},
  KVSpec = #{id => kv_server, start => {kv, start_link, []}},
  ETSKVSpec = #{id => kv_server_ets, start => {kv_ets, start_link, []}},
  WorkerSupSpec = 
    #{ id => worker_sup, 
       start => {worker_sup, start_link, []},
       type => supervisor
    },
  Children = [KVSpec, ETSKVSpec, WorkerSupSpec],
  {ok, {SupFlags, Children}}.
