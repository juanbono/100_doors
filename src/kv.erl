-module(kv).

-behaviour(gen_server).

-export([start_link/0, put/2, get/1]).

-export([init/1, handle_call/3, handle_cast/2]).

% API
start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

put(Key, Value) ->
  gen_server:cast(?MODULE, {put, Key, Value}).

get(Key) ->
  gen_server:call(?MODULE, {get, Key}).

% Callbacks

init([]) ->
  {ok, maps:new()}.

handle_call({get, Key}, _From, State) ->
  case maps:get(Key, State) of 
    {badmap, Reason} ->
      {reply, {error, Reason}, State};
    Value ->
      {reply, Value, State}
  end.

handle_cast({put, Key, Value}, State) ->
  NewState = maps:put(Key, Value, State),  
  {noreply, NewState}.
