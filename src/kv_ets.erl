-module(kv_ets).

-behaviour(gen_server).

-export([start_link/0, put/2, get/1]).

-export([init/1, handle_call/3, handle_cast/2]).

% API
start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

put(Key, Value) ->
  gen_server:cast(?MODULE, {put, Key, Value}).

get(Key) ->
  gen_server:call(?MODULE, {get, Key}).

% Callbacks
init([]) ->
  ets:new(state, [ordered_set, named_table]),
  {ok, []}.

handle_call({get, Key}, _From, State) ->
  try
    Value = ets:lookup_element(state, Key, 2),
    {reply, Value, State}
  catch
    Class:Exception:StackTrace ->
      io:format("Class=~p~nException=~p~nStackTrace=~p~n", [Class, Exception, StackTrace]),
      {reply, {error, Exception}, State}
  end.

handle_cast({put, Key, Value}, State) ->
  ets:insert(state, {Key, Value}),  
  {noreply, State}.
