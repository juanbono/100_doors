-module(doors).

% There are 100 doors in a row that are all initially closed.
% You make 100 passes by the doors.
% The first time through, visit every door and toggle the door (if the door is closed, open it; if it is open, close it).
% The second time, only visit every 2nd door   (door #2, #4, #6, ...), and toggle it.
% The third time, visit every 3rd door (door #3, #6, #9, ...), etc, until you only visit the 100th door. 

-behaviour(gen_server).

-export([start/0]).

-export([toggle/2, start_link/1]).

-export([init/1, handle_cast/2, handle_call/3]).

start() ->
    Doors = [ start_link(X) || X <- lists:seq(1, 100) ],
    Pids = lists:map(fun({ok, Pid}) -> Pid end, Doors), 
    [lists:foreach(fun(Pid) -> toggle(Pid, X) end, Pids) || X <-  lists:seq(1, 100)].

start_link(X) ->
    gen_server:start_link(?MODULE, [X], []).

toggle(Pid, Num) ->
    gen_server:cast(Pid, {toggle, Num}).

init([X]) ->
    {ok, #{number => X, state => closed}}.

handle_cast({toggle, Num}, #{number := DoorNum} = State) ->
    case DoorNum rem Num =:= 0 of
        true -> {noreply, change(State)};
        false -> {noreply, State}
    end.

handle_call(_Msg, _From, #{state := State}) ->
    {reply, ok, State}.

change(#{state := closed} = State) -> maps:put(state, open, State);
change(#{state := open} = State)   -> maps:put(state, closed, State).

