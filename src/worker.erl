-module(worker).

-behaviour(gen_server).

-export([start_link/1]).

-export([init/1, handle_cast/2, handle_call/3]).

start_link(Num) ->
  io:format("~p~n", [Num]),
  gen_server:start_link(?MODULE, [], []).

init([]) ->
  {ok, []}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_call(_Msg, _From, State) ->
  {reply, ok, State}.
